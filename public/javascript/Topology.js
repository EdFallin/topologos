/**/

import {Dom} from "./dom.js";

export class Topology {
    constructor(dom) {
        this.dom = dom;
    }

    init() {
        this.initStyleMap();
        this.wireEvents();
        this.displaySelectedStyle({ target: { value: "Graph" }});
    }

    initStyleMap() {
        let passer = new Map();
        passer.set("Graph", this.dom.asGraph);
        passer.set("Text", this.dom.asText);

        this.stylesByName = passer;
    }

    wireEvents() {
        this.dom.displayStyle.addEventListener("change", (e) => {
            this.displaySelectedStyle(e);
        });
    }

    displaySelectedStyle(e) {
        this.hideBothStyleZones();
        let toDisplay = this.getNamedZone(e.target.value);
        this.unhideZone(toDisplay);
    }

    hideBothStyleZones() {
        for (let div of this.dom.asStyles) {
            div.hidden = true;
        }
    }

    getNamedZone(name) {
        let toDisplay = this.stylesByName.get(name);
        return toDisplay;
    }

    unhideZone(el) {
        el.hidden = false;
    }
}
