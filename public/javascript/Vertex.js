/**/

/* The contents of each vertex / node in the graph.  Edges to other vertices   
 * are maintained, in a near-standard graph implemn, in an array of lists.  */
export class Vertex {
    static get noId() /* passed */ {
        return -1;
    }

    constructor(name, text, id) /* passed */ {
        if (id) {
            this._id = id;
        }
        if (name) {
            this._name = name;
        }
        if (text) {
            this._text = text;
        }
    }

    // region .id

    get id() /* passed */ {
        return this._id || Vertex.noId;
    }

    set id(id) /* passed */ {
        this._id = id;
    }

    // endregion .id

    // region .name

    get name() /* passed */ {
        return this._name || null;
    }

    set name(name) /* passed */ {
        this._name = name;
    }

    // endregion .name

    // region .text

    get text() /* passed */ {
        return this._text || null;
    }

    set text(text) /* passed */ {
        this._text = text;
    }

    // endregion .text

}
