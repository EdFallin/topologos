/**/

//* an undirected labeled graph; adjacency-list implementation *//
export class Graph {
    // region constructing

    constructor() {
        // the actual graph: named vertices;
        // edge arrays holding vertex indices
        this.vertices = [];
        this.edges = [];

        // for traversals
        this.visits = [];
    }

    static fromContents(vertices, edges, visits) {
        let graph = new Graph();
        graph.vertices = vertices;
        graph.edges = edges;
        graph.visits = visits;

        return graph;
    }

    // endregion constructing


    // region adding

    addVertex(vertex, at) /* passed */ {
        // if vertex exists, don't add it again
        let foundAt = this.vertices.findIndex(x => x.id === vertex.id);

        if (foundAt !== -1) {
            return;
        }

        // actually adding; splice() normalizes ``at if it's 
        // out of the valid index range or even undefined  
        this.vertices.splice(at, 0, vertex);
        this.edges.splice(at, 0, []);

        // increment far ends of edges for 
        // vertices that are now 1 higher 
        for (let edges of this.edges) {
            for (let of = 0; of < edges.length; of++) {
                if (edges[of] >= at) {
                    edges[of]++;
                }
            }
        }
    }

    addEdge(from, to) {
        // edges that exist already aren't repeated 
        if (this.edges[from].includes(to)) {
            return;
        }

        // actually adding edge, 
        // undirected so twice 
        this.edges[from].push(to);
        this.edges[to].push(from);
    }

    // endregion adding


    // region removing

    removeVertex(name) {
        let index = BinarySearch.searchExact(this.vertices, name);
        let edges = this.edges[index];

        // decrementing all edge indices everywhere that are higher than
        // the removed index, including those on the removed vertex,
        // since those have to be traversed later to be removed
        for (let farEnds of this.edges) {
            for (let at = 0; at < farEnds.length; at++) {
                if (farEnds[at] > index) {
                    farEnds[at]--;
                }
            }
        }

        // deleting the vertex and its own edges;
        // splice() with two args performs deletion
        this.vertices.splice(index, 1);
        this.edges.splice(index, 1);

        // deleting edges to the vertex, found at the edges' far ends
        for (let farEnd of edges) {
            BinaryDelete.delete(this.edges[farEnd], index);
        }
    }

    removeEdge(from, to) {
        // everything is done by indices
        let fromIndex = BinarySearch.searchExact(this.vertices, from);
        let toIndex = BinarySearch.searchExact(this.vertices, to);

        // undirected graph, so each edge must be removed twice
        BinaryDelete.delete(this.edges[fromIndex], toIndex);
        BinaryDelete.delete(this.edges[toIndex], fromIndex);
    }

    // endregion removing


    // region finding

    findVertex(name) {
        // the actual finding
        let index = BinarySearch.searchExact(this.vertices, name);

        // failed search: nothing meaningful to return
        if (index === -1) {
            return null;
        }

        // successful search: vertex and its edges together
        return {index, edges: this.edges[index]};
    }

    findVertices(...names) {
        let soughtIndices = [];
        let foundIndices = [];

        // indices are needed for all names, to find them in edge arrays
        for (let name of names) {
            let index = BinarySearch.searchExact(this.vertices, name);

            // an invalid vertex name is ignored
            if (index === -1) {
                continue;
            }

            // valid vertices are retained
            soughtIndices.push(index);
        }

        // sorting the vertex indices by the number of associated edges,
        // to reduce the total time spent binary-searching edge arrays
        soughtIndices.sort((a, b) => this.edges[a].length - this.edges[b].length);

        // dequeueing the starting index
        let firstIndex = soughtIndices.shift();

        // looking at all neighbors of first vertex
        for (let neighbor of this.edges[firstIndex]) {
            // getting at all neighbors from this neighbor of first index
            let edgesFromNeighbor = this.edges[neighbor];
            let doesMatchAll = true;

            // looking for all of the input vertices at the neighbor;
            // if any is missing, this isn't one of the output vertices
            for (let index of soughtIndices) {
                let at = BinarySearch.searchExact(edgesFromNeighbor, index);

                if (at === -1) {
                    doesMatchAll = false;
                    break;
                }
            }

            // if a neighbor matches all, it's one of the soughts
            if (doesMatchAll) {
                foundIndices.push(neighbor);
            }
        }

        // all of the soughts are returned, along with their edges
        let founds = [];

        // gathering output
        for (let found of foundIndices) {
            founds.push({name: this.vertices[found], edges: this.edges[found]});
        }

        // output
        return founds;
    }

    // endregion finding


    /* also needed:
       any refactoring or optimizing I can do;
       possibly converting the arrays to a map;
    */
}
