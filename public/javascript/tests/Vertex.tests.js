/**/

import {expect} from "chai";

import {Vertex} from "../Vertex.js";

describe("Vertex", () => {
    // region Static .noId

    describe(".noId static", () => {
        it("Returns -1 when retrieved.", /* working */ () => {
            /* Arrange. */
            let expected = -1;

            /* Act. */
            let actual = Vertex.noId;

            /* Assert. */
            expect(actual).to.equal(expected);
        });
    });

    // endregion Static .noId

    // region constructor()
    
    describe("constructor()", () => {
        it("Sets .name when a name is provided.", /* working */ () => {
            /* Arrange. */
            let expected = "Vertex Name";

            /* Act. */
            let instance = new Vertex(expected);

            /* Assert. */
            expect(instance.name).to.equal(expected);
        });

        it("Doesn't set .name when no name is provided.", /* working */ () => {
            /* No arrange. */

            /* Act. */
            let instance = new Vertex();

            /* Assert. */
            // If it's not set, .name should return its default.
            expect(instance.name).to.be.null;
        });

        it("Sets .text when a text is provided.", /* working */ () => {
            /* Arrange. */
            let expected = "Vertex Text";

            /* Act. */
            let instance = new Vertex("Vertex Name", expected);

            /* Assert. */
            expect(instance.text).to.equal(expected);
        });

        it("Doesn't set .text when no text is provided.", /* working */ () => {
            /* No arrange. */

            /* Act. */
            let instance = new Vertex("Vertex Name");

            /* Assert. */
            // If it's not set, .text should return its default.
            expect(instance.text).to.be.null;
        });

        it("Sets .id when an id is provided.", /* working */ () => {
            /* Arrange. */
            let expected = 77;
            
            /* Act. */
            let instance = new Vertex("name", "text", expected);
            
            /* Assert. */
            expect(instance.id).to.equal(expected);
        });

        it("Doesn't set .id when no id is provided.", /* working */ () => {
            /* Arrange. */
            let expected = 77;

            /* Act. */
            let instance = new Vertex("name", "text");

            /* Assert. */
            // If it's not set, .id should return its default.
            expect(instance.id).to.equal(Vertex.noId);
        });
    });
    
    // endregion constructor()
    
    // region .id

    describe(".id", () => {
        it("Returns Vertex.noId when no value has been set.", /* working */ () => {
            /* Arrange. */
            let instance = new Vertex();

            /* Act. */
            let actual = instance.id;

            /* Assert. */
            expect(actual).to.equal(Vertex.noId);
        });

        it("Returns the value it has previously been set to.", /* working */ () => {
            /* Arrange. */
            let instance = new Vertex();
            let expected = 27;

            /* Act. */
            // In essence, both of these are tested here.
            instance.id = expected;
            let actual = instance.id;

            /* Assert. */
            expect(actual).to.equal(expected);
        });
    });

    // endregion .id

    // region .name

    describe(".name", () => {
        it("Returns null when no value has been set.", /* working */ () => {
            /* Arrange. */
            let instance = new Vertex();

            /* Act. */
            let actual = instance.name;

            /* Assert. */
            expect(actual).to.be.null;
        });

        it("Returns the value it has previously been set to.", /* working */ () => {
            /* Arrange. */
            let instance = new Vertex();
            let expected = "Vertex Name";

            /* Act. */
            // In essence, both of these are tested here.
            instance.name = expected;
            let actual = instance.name;

            /* Assert. */
            expect(actual).to.equal(expected);
        });
    });

    // endregion .name

    // region .text

    describe(".text", () => {
        it("Returns null when no value has been set.", /* working */ () => {
            /* Arrange. */
            let instance = new Vertex();

            /* Act. */
            let actual = instance.text;

            /* Assert. */
            expect(actual).to.be.null;
        });

        it("Returns the value it has previously been set to.", /* working */ () => {
            /* Arrange. */
            let instance = new Vertex();
            let expected = "Vertex Text";

            /* Act. */
            // In essence, both of these are tested here.
            instance.text = expected;
            let actual = instance.text;

            /* Assert. */
            expect(actual).to.equal(expected);
        });
    });

    // endregion .text
});
