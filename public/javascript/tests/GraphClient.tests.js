/**/

// import {TestKit} from "./TestKit.js";
import {GraphClient} from "../GraphClient.js";

export class GraphClientTests {
    // region crux

    constructor() {
        this.kit = new TestKit();
    }

    * run() {
        // build list of tests, run the tests
        this.define();
        yield* this.kit.run();
    }

    // endregion crux


    // region tests

    define() {
        this.kit.group("GraphClient",
            () => {
                // region fromFactorsToBundle()

                this.kit.test("fromFactorsToBundle",
                    "When given known vertex-only factors, the correct bundle contents are returned.",
                    () => {
                        //* arrange *//
                        let client = new GraphClient();

                        let factors = ["a", "bee", "sea"];

                        let expected = {
                            nodes: [
                                {vertex: "a", edges: []},
                                {vertex: "bee", edges: []},
                                {vertex: "sea", edges: []}
                            ]
                        };

                        //* act *//
                        let actual = client.fromFactorsToBundle(factors);


                        //* assert *//
                        expect(actual.nodes.length).to.equal(expected.nodes.length);

                        let expecteds = expected.nodes;
                        let actuals = actual.nodes;

                        for (let at = 0; at < expecteds.length; at++) {
                            expect(actuals[at].vertex).to.equal(expecteds[at].vertex);
                            expect(actuals[at].edges.length).to.equal(expecteds[at].edges.length);
                        }
                    }
                );

                // endregion fromFactorsToBundle()

                // region fromFactorsToUriSegment()

                this.kit.test("fromFactorsToUriSegment",
                    "When given known vertex factors, the correct HTTP-friendly output text is returned.",
                    () => {
                        //* arrange *//
                        let client = new GraphClient();

                        let factors = ["first", "two", "C"];

                        let expected = "%5B%22first%22%2C%22two%22%2C%22C%22%5D";

                        //* act *//
                        let actual = client.fromFactorsToUriSegment(factors);

                        //* assert *//
                        expect(actual).to.equal(expected);
                    }
                );

                this.kit.test("fromFactorsToUriSegment",
                    "When given known vertex factors, the output can be converted back to the input.",
                    () => {
                        //* arrange *//
                        let client = new GraphClient();

                        let expected = ["first", "two", "3"];

                        //* act *//
                        let acSegment = client.fromFactorsToUriSegment(expected);

                        //* assemble *//
                        let acJson = decodeURIComponent(acSegment);
                        let actual = JSON.parse(acJson);

                        //* assert *//
                        expect(Array.isArray(actual)).to.equal(true);

                        expect(actual.length).to.equal(expected.length);

                        for (let at = 0; at < expected.length; at++) {
                            expect(actual[at]).to.equal(expected[at]);
                        }
                    }
                );

                // endregion fromFactorsToUriSegment()
            }
        );
    }

    // endregion tests
}
