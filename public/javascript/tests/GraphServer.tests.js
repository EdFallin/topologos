/**/

// import {TestKit} from "./TestKit.js";
import {Graph} from "../Graph.js";
import {GraphServer} from "../GraphServer.js";

import fs from "fs";
import path from "path";

export class GraphServerTests {
    // region crux

    constructor() {
        this.kit = new TestKit();
    }

    * run() {
        // build list of tests, run the tests
        this.define();
        yield* this.kit.run();
    }

    // endregion crux


    // region tests

    define() {
        this.kit.group("GraphServer",
            () => {
                // region get graphSite()

                this.kit.test("get graphSite",
                    "When the graph site is retrieved with known components, it has the correct whole contents.",
                    () => {
                        //* arrange *//
                        // getting the actual current path
                        let moduleUrl = import.meta.url;
                        let url = new URL(moduleUrl);
                        let __dirName = path.dirname(url.pathname);

                        let state = new GraphServer("TestGraphFile.json");

                        // in some environments, __dirName may begin with a forward slash followed
                        // by a drive letter; if so, that slash is removed so the path is usable
                        let driveLetterStyle = /\/\w:/;

                        if (driveLetterStyle.test(__dirName)) {
                            __dirName = __dirName.substring(1);
                        }

                        let expected = path.join(/*__dirName,*/ state.graphFolder, state.graphFile);

                        //* act *//
                        let actual = state.graphSite;

                        //* assert *//
                        expect(actual).to.equal(expected);
                    }
                );

                // endregion get graphSite()

                // region fromFile()

                this.kit.test("fromFile",
                    "When no file exists, the file is created containing an empty Graph.",
                    () => {
                        //* arrange *//
                        let file = "MissingTestGraph.json";
                        let state = new GraphServer(file);

                        // ensuring the file isn't there; unlinking is deleting
                        if (fs.existsSync(state.graphSite)) {
                            fs.unlinkSync(state.graphSite);
                        }

                        //* act *//
                        GraphServer.fromFile(file);

                        //* assemble *//
                        let actual = fs.existsSync(state.graphSite);
                        let acGraph = null;

                        // getting the Graph from the contents of the file
                        if (fs.existsSync(state.graphSite)) {
                            let asJson = fs.readFileSync(state.graphSite, "utf-8");
                            let asObject = JSON.parse(asJson);
                            acGraph = Graph.fromContents(asObject.vertices, asObject.edges, asObject.visits);
                        }

                        //* clean up *//
                        fs.unlinkSync(state.graphSite);

                        //* assert *//
                        expect(actual).to.equal(true);

                        // first, type metadata for GraphServer and Graph
                        expect(acGraph instanceof Graph).to.equal(true);

                        // then, content metadata; no contents to traverse and equate
                        expect(acGraph.vertices.length).to.equal(0);
                        expect(acGraph.edges.length).to.equal(0);
                        expect(acGraph.visits.length).to.equal(0);
                    }
                );

                this.kit.test("fromFile",
                    "When no file exists, the method returns a GraphServer with an empty Graph.",
                    () => {
                        //* arrange *//
                        let file = "MissingTestGraph.json";
                        let state = new GraphServer(file);

                        // ensuring the file isn't there; unlinking is deleting
                        if (fs.existsSync(state.graphSite)) {
                            fs.unlinkSync(state.graphSite);
                        }

                        //* act *//
                        let acServer = GraphServer.fromFile(file);

                        //* assemble *//
                        let actual = acServer.graph;

                        //* clean up *//
                        fs.unlinkSync(state.graphSite);

                        //* assert *//
                        // first, type metadata for GraphServer and Graph
                        expect(acServer instanceof GraphServer).to.equal(true);
                        expect(actual instanceof Graph).to.equal(true);

                        // then, content metadata; no contents to traverse and equate
                        expect(actual.vertices.length).to.equal(0);
                        expect(actual.edges.length).to.equal(0);
                        expect(actual.visits.length).to.equal(0);
                    }
                );

                this.kit.test("fromFile",
                    "When a file contains known Graph contents, the method returns a GraphServer with the correct Graph.",
                    () => {
                        //* arrange *//
                        let file = "TestGraph.json";
                        let state = new GraphServer(file);

                        // a simple example graph
                        let expected = new Graph();
                        expected.addVertex("a");
                        expected.addVertex("b");
                        expected.addVertex("c");
                        expected.addEdge("a", "b");
                        expected.addEdge("b", "c");

                        state.graph = expected;

                        // writing the example to the file
                        let asJson = JSON.stringify(expected);
                        fs.writeFileSync(state.graphSite, asJson);

                        //* act *//
                        let acState = GraphServer.fromFile(file);

                        //* assemble *//
                        let actual = acState.graph;

                        //* clean up *//
                        // "unlinking" means deleting
                        fs.unlinkSync(state.graphSite);

                        //* assert *//
                        // an object can't be directly equated by contents,
                        // so instead, I'm comparing all the contents

                        // first, type metadata for GraphServer and Graph
                        expect(acState instanceof GraphServer).to.equal(true);
                        expect(actual instanceof Graph).to.equal(true);

                        // then, content metadata
                        expect(actual.vertices.length).to.equal(expected.vertices.length);
                        expect(actual.edges.length).to.equal(expected.edges.length);
                        expect(actual.visits.length).to.equal(expected.visits.length);

                        // then all the individual contents:

                        // first contents, the vertices, a plain array
                        for (let at = 0; at < expected.vertices.length; at++) {
                            expect(actual.vertices[at]).to.equal(expected.vertices[at]);
                        }

                        // second contents, the edges, an array of arrays
                        for (let at = 0; at < expected.edges.length; at++) {
                            // edges are arrays whose lengths may vary
                            expect(actual.edges[at].length).to.equal(expected.edges[at].length);

                            // the actual edges within the array for each vertex
                            for (let of = 0; of < expected.edges[at].length; of++) {
                                expect(actual.edges[at][of]).to.equal(expected.edges[at][of]);
                            }
                        }

                        // third contents, the visits, a plain array
                        for (let at = 0; at < expected.visits.length; at++) {
                            expect(actual.visits[at]).to.equal(expected.visits[at]);
                        }
                    }
                );

                // endregion fromFile()

                // region writeGraphToFile()

                this.kit.test("writeGraphToFile",
                    "When a known Graph is written to file, the same Graph is retained on a GraphServer read from that file.",
                    () => {
                        //* arrange *//
                        let file = "TestGraph.json";
                        let expected = new Graph();

                        let state = new GraphServer(file);
                        state.graph = expected;

                        // a simple example graph, the same as used in the read-file test,
                        // changed after adding to state to ensure mutability works
                        expected.addVertex("a");
                        expected.addVertex("b");
                        expected.addVertex("c");
                        expected.addEdge("a", "b");
                        expected.addEdge("b", "c");

                        //* act *//
                        state.writeGraphToFile();

                        //* assemble results *//
                        // getting a GraphServer, then a Graph
                        let acState = GraphServer.fromFile(file);
                        let actual = acState.graph;

                        //* clean up *//
                        // "unlinking" means deleting
                        fs.unlinkSync(state.graphSite);

                        //* assert *//
                        // an object can't be directly equated by contents,
                        // so instead, I'm comparing all the contents

                        // first, type metadata for GraphServer and Graph
                        expect(acState instanceof GraphServer).to.equal(true);
                        expect(actual instanceof Graph).to.equal(true);

                        // then, content metadata
                        expect(actual.vertices.length).to.equal(expected.vertices.length);
                        expect(actual.edges.length).to.equal(expected.edges.length);
                        expect(actual.visits.length).to.equal(expected.visits.length);

                        // then all the individual contents:

                        // first contents, the vertices, a plain array
                        for (let at = 0; at < expected.vertices.length; at++) {
                            expect(actual.vertices[at]).to.equal(expected.vertices[at]);
                        }

                        // second contents, the edges, an array of arrays
                        for (let at = 0; at < expected.edges.length; at++) {
                            // edges are arrays whose lengths may vary
                            expect(actual.edges[at].length).to.equal(expected.edges[at].length);

                            // the actual edges within the array for each vertex
                            for (let of = 0; of < expected.edges[at].length; of++) {
                                expect(actual.edges[at][of]).to.equal(expected.edges[at][of]);
                            }
                        }

                        // third contents, the visits, a plain array
                        for (let at = 0; at < expected.visits.length; at++) {
                            expect(actual.visits[at]).to.equal(expected.visits[at]);
                        }
                    }
                );

                // endregion writeGraphToFile()

                // region find()

                this.kit.test("find",
                    "When a sought vertex doesn't exist, an empty array is returned.",
                    () => {
                        //* arrange *//
                        let graph = this.makeSimpleGraphForSingleFind();
                        let server = new GraphServer("");
                        server.graph = graph;

                        let factors = ["q"];

                        let expected = [];

                        //* act *//
                        let actual = server.find(factors);

                        //* assert *//
                        expect(actual.length).to.equal(expected.length);
                    }
                );

                this.kit.test("find",
                    "When a sought vertex exists, its name and edges are returned in a one-element array.",
                    () => {
                        //* arrange *//
                        let graph = this.makeSimpleGraphForSingleFind();
                        let server = new GraphServer("");
                        server.graph = graph;

                        let factors = ["d"];

                        // "d" is at 3 and has edges to "a" at 0 and "h" at 7
                        let expected = {name: "d", edges: [0, 7]};

                        //* act *//
                        let actuals = server.find(factors);

                        //* assert *//
                        expect(actuals.length).to.equal(1);

                        let actual = actuals[0];
                        expect(actual.name).to.equal(expected.name);
                        expect(actual.edges.length).to.equal(expected.edges.length);

                        for (let of = 0; of < expected.edges.length; of++) {
                            expect(actual.edges[of]).to.equal(expected.edges[of]);
                        }
                    }
                );

                this.kit.test("find",
                    "When vertices are sought by two other vertices, the correct vertices are returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph,
                            "a", "b", "c", "d", "e", "f", "g", "h"
                        );

                        // b neighbors a, f, and h; e neighbors a and h;
                        // vertices a and h are neighbors of both b and e
                        this.addGraphEdges(graph,
                            {from: "b", to: "a"},
                            {from: "b", to: "f"},
                            {from: "b", to: "h"}
                        );
                        this.addGraphEdges(graph,
                            {from: "e", to: "a"},
                            {from: "e", to: "h"}
                        );

                        let expecteds = [{name: "a", edges: [1, 4]}, {name: "h", edges: [1, 4]}];

                        let server = new GraphServer("");
                        server.graph = graph;

                        let factors = ["b", "e"];

                        //* act *//
                        let actuals = server.find(factors);

                        //* assert *//
                        // the right number of vertices should have been returned
                        expect(actuals.length).to.equal(expecteds.length);

                        // each of the vertices returned should have the right name and edges
                        for (let of = 0; of < expecteds.length; of++) {
                            // name
                            expect(actuals[of].name).to.equal(expecteds[of].name);
                            expect(actuals[of].edges.length).to.equal(expecteds[of].edges.length);

                            // localizing edges
                            let expEdges = expecteds[of].edges;
                            let acEdges = actuals[of].edges;

                            // edges
                            for (let at = 0; at < expEdges.length; at++) {
                                expect(acEdges[at]).to.equal(expEdges[at]);
                            }
                        }
                    }
                );


                this.kit.test("find",
                    "When vertices are sought by three other vertices, the correct vertices are returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        this.addGraphVertices(graph,
                            "a", "b", "c", "d", "e", "f", "g",
                            "h", "i", "j", "k", "l", "m"
                        );

                        // b neighbors a, f, g, and h
                        this.addGraphEdges(graph,
                            {from: "b", to: "a"}, {from: "b", to: "f"},
                            {from: "b", to: "g"}, {from: "b", to: "h"}
                        );

                        // e neighbors a, c, g, and h
                        this.addGraphEdges(graph,
                            {from: "e", to: "a"}, {from: "e", to: "c"},
                            {from: "e", to: "g"}, {from: "e", to: "h"}
                        );

                        // d neighbors a, and g
                        graph.addEdge("d", "a");
                        graph.addEdge("d", "g");

                        // a few extra neighbors for fuller edge arrays in output, including between the founds
                        this.addGraphEdges(graph,
                            {from: "a", to: "j"}, {from: "g", to: "l"},
                            {from: "a", to: "k"}, {from: "g", to: "m"},
                            {from: "a", to: "i"}, {from: "a", to: "g"}
                        );

                        let server = new GraphServer("");
                        server.graph = graph;

                        let factors = ["e", "d", "b"];

                        // a and g are the only neighbors of all of b, e, and d;
                        // their edges include all their neighbors, sought or not
                        let expecteds = [
                            {name: "a", edges: [1, 3, 4, 6, 8, 9, 10]},
                            {name: "g", edges: [0, 1, 3, 4, 11, 12]}
                        ];

                        //* act *//
                        let actuals = server.find(factors);

                        //* assert *//
                        // the right number of vertices should have been returned
                        expect(actuals.length).to.equal(expecteds.length);

                        // each of the vertices returned should have the right name and edges
                        for (let of = 0; of < expecteds.length; of++) {
                            // name
                            expect(actuals[of].name).to.equal(expecteds[of].name);
                            expect(actuals[of].edges.length).to.equal(expecteds[of].edges.length);

                            // localizing edges
                            let expEdges = expecteds[of].edges;
                            let acEdges = actuals[of].edges;

                            // edges
                            for (let at = 0; at < expEdges.length; at++) {
                                expect(acEdges[at]).to.equal(expEdges[at]);
                            }
                        }
                    }
                );

                // endregion find()

                // region add()

                this.kit.test("add",
                    "When only a vertex is added, it is added to the graph: it can be found again.",
                    () => {
                        //* arrange *//
                        let graphFile = "AddGraph.json";
                        let namer = new GraphServer(graphFile);

                        // deleting any existing file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        // creating new GraphServer and :. file
                        let server = GraphServer.fromFile(graphFile);

                        let body = {
                            "nodes": [{"vertex": "a", "edges": []}]
                        };

                        let expected = {name: "a", edges: []};

                        //* act *//
                        server.add(body);

                        //* assemble *//
                        // calling find()
                        let actuals = server.find("a");
                        let actual = actuals[0];

                        //* clean up *//
                        // deleting the file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        //* assert *//
                        expect(actual.name).to.equal(expected.name);
                        expect(actual.edges.length).to.equal(expected.edges.length);

                        for (let of = 0; of < expected.edges.length; of++) {
                            expect(actual.edges[of]).to.equal(expected.edges[of]);
                        }
                    }
                );

                this.kit.test("add",
                    "When only vertices are added, they are added to the graph: they can be found again.",
                    () => {
                        //* arrange *//
                        let graphFile = "AddGraph.json";
                        let namer = new GraphServer(graphFile);

                        // deleting any existing file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        // creating new GraphServer and :. file
                        let server = GraphServer.fromFile(graphFile);

                        let body = {
                            "nodes": [
                                {"vertex": "a", "edges": []},
                                {"vertex": "b", "edges": []},
                                {"vertex": "c", "edges": []}
                            ]
                        };

                        let expA = {name: "a", edges: []};
                        let expB = {name: "b", edges: []};
                        let expC = {name: "c", edges: []};

                        //* act *//
                        server.add(body);

                        //* assemble *//
                        // calling find()
                        let acA = server.find("a");
                        let acB = server.find("b");
                        let acC = server.find("c");

                        // extracting from one-item arrays
                        acA = acA[0];
                        acB = acB[0];
                        acC = acC[0];

                        // gathering for traversal
                        let expecteds = [expA, expB, expC];
                        let actuals = [acA, acB, acC];

                        //* clean up *//
                        // deleting the file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        //* assert *//
                        // traversing and equating each vertex's expected-actual pairing
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            expect(actual.name).to.equal(expected.name);
                            expect(actual.edges.length).to.equal(expected.edges.length);

                            for (let of = 0; of < expected.edges.length; of++) {
                                expect(actual.edges[of]).to.equal(expected.edges[of]);
                            }
                        }
                    }
                );

                this.kit.test("add",
                    "When a vertex and associated edges are added, they are added to the graph: they can be found again together.",
                    () => {
                        //* arrange *//
                        let graphFile = "AddGraph.json";
                        let namer = new GraphServer(graphFile);

                        // deleting any existing file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        // creating new GraphServer and :. file
                        let server = GraphServer.fromFile(graphFile);

                        // adding some existing vertices to have edges to
                        server.graph.addVertex("a");
                        server.graph.addVertex("b");

                        // the new vertex has edges to both "a" and "b"
                        let body = {
                            "nodes": [{"vertex": "c", "edges": ["a", "b"]}]
                        };

                        let expFromA = {name: "a", edges: [2]};
                        let expFromB = {name: "b", edges: [2]};
                        let expFromC = {name: "c", edges: [0, 1]};

                        //* act *//
                        server.add(body);

                        //* assemble *//
                        // calling find() from each vertex
                        let acFromA = server.find("a");
                        let acFromB = server.find("b");
                        let acFromC = server.find("c");

                        // extracting from one-item arrays
                        acFromA = acFromA[0];
                        acFromB = acFromB[0];
                        acFromC = acFromC[0];


                        // gathering expecteds and actuals
                        let expecteds = [expFromA, expFromB, expFromC];
                        let actuals = [acFromA, acFromB, acFromC];

                        //* clean up *//
                        // deleting the file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            expect(actual.name).to.equal(expected.name);
                            expect(actual.edges.length).to.equal(expected.edges.length);

                            for (let of = 0; of < expected.edges.length; of++) {
                                expect(actual.edges[of]).to.equal(expected.edges[of]);
                            }
                        }
                    }
                );

                this.kit.test("add",
                    "When vertices and their edges are added, they are added to the graph: they can be found again together.",
                    () => {
                        //* arrange *//
                        let graphFile = "AddGraph.json";
                        let namer = new GraphServer(graphFile);

                        // deleting any existing file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        // creating new GraphServer and :. file
                        let server = GraphServer.fromFile(graphFile);

                        // adding some existing vertices to have edges to
                        server.graph.addVertex("a");
                        server.graph.addVertex("b");

                        // adding all three vertices,
                        // with edges a-b, a-c, b-c
                        let body = {
                            "nodes": [
                                {"vertex": "a", "edges": []},
                                {"vertex": "b", "edges": ["a"]},
                                {"vertex": "c", "edges": ["a", "b"]}
                            ]
                        };

                        let expFromA = {name: "a", edges: [1, 2]};
                        let expFromB = {name: "b", edges: [0, 2]};
                        let expFromC = {name: "c", edges: [0, 1]};

                        //* act *//
                        server.add(body);

                        //* assemble *//
                        // calling find() from each vertex
                        let acFromA = server.find("a");
                        let acFromB = server.find("b");
                        let acFromC = server.find("c");

                        // extracting from one-item arrays
                        acFromA = acFromA[0];
                        acFromB = acFromB[0];
                        acFromC = acFromC[0];

                        // gathering expecteds and actuals
                        let expecteds = [expFromA, expFromB, expFromC];
                        let actuals = [acFromA, acFromB, acFromC];

                        //* clean up *//
                        // deleting the file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            expect(actual.name).to.equal(expected.name);
                            expect(actual.edges.length).to.equal(expected.edges.length);

                            for (let of = 0; of < expected.edges.length; of++) {
                                expect(actual.edges[of]).to.equal(expected.edges[of]);
                            }
                        }
                    }
                );

                this.kit.test("add",
                    "When vertices and edges are added, they are saved to the file: a new GraphServer using the same file contains them.",
                    () => {
                        //* arrange *//
                        let graphFile = "AddGraph.json";
                        let namer = new GraphServer(graphFile);

                        // deleting any existing file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        // creating new GraphServer and :. file
                        let server = GraphServer.fromFile(graphFile);

                        // adding some existing vertices to have edges to
                        server.graph.addVertex("a");
                        server.graph.addVertex("b");

                        // adding all three vertices,
                        // with edges a-b, a-c, b-c
                        let body = {
                            "nodes": [
                                {"vertex": "a", "edges": []},
                                {"vertex": "b", "edges": ["a"]},
                                {"vertex": "c", "edges": ["a", "b"]}
                            ]
                        };

                        let expFromA = {name: "a", edges: [1, 2]};
                        let expFromB = {name: "b", edges: [0, 2]};
                        let expFromC = {name: "c", edges: [0, 1]};

                        //* act *//
                        server.add(body);

                        //* assemble *//
                        // getting an all-new GraphServer and calling find() on it
                        server = null;
                        let stored = GraphServer.fromFile(graphFile);

                        let acFromA = stored.find("a");
                        let acFromB = stored.find("b");
                        let acFromC = stored.find("c");

                        // extracting from one-item arrays
                        acFromA = acFromA[0];
                        acFromB = acFromB[0];
                        acFromC = acFromC[0];

                        // gathering expecteds and actuals
                        let expecteds = [expFromA, expFromB, expFromC];
                        let actuals = [acFromA, acFromB, acFromC];

                        //* clean up *//
                        // deleting the file
                        if (fs.existsSync(namer.graphSite)) {
                            fs.unlinkSync(namer.graphSite);
                        }

                        //* assert *//
                        for (let at = 0; at < expecteds.length; at++) {
                            let expected = expecteds[at];
                            let actual = actuals[at];

                            expect(actual.name).to.equal(expected.name);
                            expect(actual.edges.length).to.equal(expected.edges.length);

                            for (let of = 0; of < expected.edges.length; of++) {
                                expect(actual.edges[of]).to.equal(expected.edges[of]);
                            }
                        }
                    }
                );

                // endregion add()

                // region remove()

                this.kit.test("remove",
                    "When only vertices are provided and they have no edges, only those vertices are removed from the Graph.",
                    () => {
                        //* arrange *//
                        let graph = this.makeComplexGraphForRemovals();
                        let server = new GraphServer();
                        server.graph = graph;

                        // test isolation; model code always writes to file
                        server.writeGraphToFile = () => {
                        };

                        let factors = [{of: "1"}, {of: "2"}, {of: "3"}, {of: "9"}];

                        let expVertices = ["4", "5", "6", "7", "8"];

                        // region calculating expected edges

                        let expEdges = [];

                        // making new arrays of each edge set so they
                        // don't auto-mutate when the graph does
                        for (let edges of graph.edges) {
                            expEdges.push(Array.from(edges));
                        }

                        // these are the edge arrays for the removed vertices,
                        // the higher one removed first for simplest indexing
                        expEdges.splice(8, 1);
                        expEdges.splice(0, 3);

                        // with the first three vertices removed,
                        // all edges have to be reduced by three
                        for (let edges of expEdges) {
                            for (let of = 0; of < edges.length; of++) {
                                edges[of] -= 3;
                            }
                        }

                        // endregion calculating expected edges

                        //* act *//
                        server.remove(factors);

                        //* assemble *//
                        let acVertices = graph.vertices;
                        let acEdges = graph.edges;

                        //* assert *//
                        this.equateEntireGraphStructure(expVertices, acVertices, expEdges, acEdges);
                    }
                );

                this.kit.test("remove",
                    "When only vertices are provided and they have edges, those vertices and the edges to and from them are removed from the Graph.",
                    () => {
                        //* arrange *//
                        let graph = this.makeComplexGraphForRemovals();
                        let server = new GraphServer();
                        server.graph = graph;

                        // test isolation; model code always writes to file
                        server.writeGraphToFile = () => {
                        };

                        let factors = [{of: "5"}, {of: "8"}];

                        let expVertices = ["1", "2", "3", "4", "6", "7", "9"];
                        let expEdges = [
                            [], [], [], [4],
                            [3], [], []
                        ];

                        //* act *//
                        server.remove(factors);

                        //* assemble *//
                        let acVertices = graph.vertices;
                        let acEdges = graph.edges;

                        //* assert *//
                        this.equateEntireGraphStructure(expVertices, acVertices, expEdges, acEdges);
                    }
                );

                this.kit.test("remove",
                    "When only edges are provided, those edges only are removed from the Graph, in both directions.",
                    () => {
                        //* arrange *//
                        let graph = this.makeComplexGraphForRemovals();
                        let server = new GraphServer();
                        server.graph = graph;

                        // test isolation; model code always writes to file
                        server.writeGraphToFile = () => {
                        };

                        // region building factors to remove

                        let factors = [];

                        // at index 3, edges should no longer have far end of 4;
                        // at index 4, edges should no longer have far end of 3
                        factors.push({from: "4", to: "5"});

                        // at index 5, edges should no longer have far end of 7;
                        // at index 7, edges should no longer have far end of 5
                        factors.push({from: "6", to: "8"});

                        // endregion building factors to remove

                        let expVertices = Array.from(graph.vertices);

                        // region calculating remaining edges

                        // since edges is a two-level array, both outer and inner
                        // must be mapped to have independent arrays to compare;
                        // equivalent to outer loop / inner from() used elsewhere
                        let expEdges = Array.from(
                            graph.edges,
                            x => Array.from(x)
                        );

                        // far ends 4 and 3 are at 0 of third and fourth inner arrays
                        expEdges[3].splice(0, 1);
                        expEdges[4].splice(0, 1);

                        // far end 7 is at 2 in 5th inner array, far end 5 is at 0 in 7th inner array

                        expEdges[5].splice(2, 1);
                        expEdges[7].splice(0, 1);

                        // endregion calculating remaining edges

                        //* act *//
                        server.remove(factors);

                        //* assemble *//
                        let acVertices = graph.vertices;
                        let acEdges = graph.edges;

                        //* assert *//
                        this.equateEntireGraphStructure(expVertices, acVertices, expEdges, acEdges);
                    }
                );

                this.kit.test("remove",
                    "When a mix of vertices and edges is provided, the correct pieces are removed from the Graph.",
                    () => {
                        //* arrange *//
                        let graph = this.makeComplexGraphForRemovals();
                        let server = new GraphServer();
                        server.graph = graph;

                        server.writeGraphToFile = () => {
                        };

                        let factors = [
                            {from: "4", to: "5"},  // drops far ends from @3 to @4 and @4 to @3
                            {of: "8"},             // also drops edges from 6 @5 and 7 @6
                            {of: "9"}              // only drops vertex @8
                        ];

                        let expVertices = ["1", "2", "3", "4", "5", "6", "7"];
                        let expEdges = [
                            [], [], [],
                            [5], [5],
                            [3, 4], []
                        ];

                        //* act *//
                        server.remove(factors);

                        //* assemble *//
                        let acVertices = graph.vertices;
                        let acEdges = graph.edges;

                        //* assert *//
                        this.equateEntireGraphStructure(expVertices, acVertices, expEdges, acEdges);
                    }
                );

                this.kit.test("remove",
                    "When a mix of vertices and edges is provided, the stored Graph has only the remaining pieces.",
                    () => {
                        //* arrange *//
                        let graph = this.makeComplexGraphForRemovals();
                        let server = new GraphServer();
                        server.graph = graph;

                        // region setting up isolated test file

                        let graphFile = "RemovalTestGraph.json";

                        server.graphFile = graphFile;
                        let graphSite = server.graphSite;

                        if (fs.existsSync(graphSite)) {
                            // unlinking is deleting
                            fs.unlinkSync(graphSite);
                        }

                        // endregion setting up isolated test file

                        //* unlike other tests, not injecting an empty file-writer here *//

                        let factors = [
                            {from: "4", to: "5"},  // drops far ends from @3 to @4 and @4 to @3
                            {of: "8"},             // also drops edges from 6 @5 and 7 @6
                            {of: "9"}              // only drops vertex @8
                        ];

                        let expVertices = ["1", "2", "3", "4", "5", "6", "7"];
                        let expEdges = [
                            [], [], [],
                            [5], [5],
                            [3, 4], []
                        ];

                        //* act *//
                        server.remove(factors);

                        //* assemble *//
                        // creating a new Graph from the file, as an all-new
                        // GraphServer's .graph, for certainty of isolation
                        server = null;
                        let acServer = GraphServer.fromFile(graphFile);
                        let actual = acServer.graph;

                        // localizing
                        let acVertices = actual.vertices;
                        let acEdges = actual.edges;

                        //* cleaning up *//
                        if (fs.existsSync(graphSite)) {
                            // unlinking is deleting
                            fs.unlinkSync(graphSite);
                        }

                        //* assert *//
                        this.equateEntireGraphStructure(expVertices, acVertices, expEdges, acEdges);
                    }
                );

                // endregion remove()

                // region topEight()

                this.kit.test("topEight",
                    "When called with known graph contents, the eight vertices with the most edges are returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        let server = new GraphServer();
                        server.graph = graph;

                        // the graph has ten vertices
                        // to choose the top eight from
                        this.addGraphVertices(
                            graph,
                            "a", "b", "c", "d", "e",
                            "f", "g", "h", "i", "j"
                        );

                        // I'm adding fake edges here,
                        // so I can be sure of the results
                        // when real edges appear twice

                        // expected order from max to min edges:
                        // d, c, b, a, i, j, g, f

                        // d, c, b, a
                        graph.edges[3] = [0, 1, 2, 4, 5, 6, 7, 8, 9, 10];
                        graph.edges[2] = [0, 1, 4, 5, 6, 7, 8, 9, 10];
                        graph.edges[1] = [0, 4, 5, 6, 7, 8, 9, 10];
                        graph.edges[0] = [4, 5, 6, 7, 8, 9, 10];

                        // i, j; discontinuous counts
                        graph.edges[8] = [4, 5, 6, 9, 10];
                        graph.edges[9] = [4, 5, 6, 10];

                        // g, f; discontinuous again
                        graph.edges[6] = [4, 5, 10];
                        graph.edges[5] = [4, 5, 10];

                        // h, i have no edges //

                        let expected = ["d", "c", "b", "a", "i", "j", "f", "g"];

                        //* act *//
                        let actual = server.topEight();

                        //* assert *//
                        for (let at = 0; at < expected.length; at++) {
                            expect(actual[at]).to.equal(expected[at]);
                        }
                    }
                );

                this.kit.test("topEight",
                    "When called with fewer than eight vertices to choose from, the empty elements are not included.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        let server = new GraphServer();
                        server.graph = graph;

                        this.addGraphVertices(
                            graph,
                            "a", "b", "c", "d"
                        );

                        this.addGraphEdges(
                            graph,
                            {from: "b", to: "d"},
                            {from: "c", to: "d"}
                        );

                        let expected = ["d", "b", "c", "a"];

                        //* act *//
                        let actual = server.topEight();

                        //* assert *//
                        expect(actual.length).to.equal(expected.length);

                        for (let at = 0; at < expected.length; at++) {
                            expect(actual[at]).to.equal(expected[at]);
                        }
                    }
                );

                this.kit.test("topEight",
                    "When called with vertices without any edges, the first eight vertices entered are returned in sorted order.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        let server = new GraphServer();
                        server.graph = graph;

                        this.addGraphVertices(
                            graph,
                            "a", "i", "c", "k", "e", "m", "g", "o",
                            "b", "j", "d", "l", "f", "n", "h", "p"
                        );

                        let expected = ["a", "b", "c", "d", "e", "f", "g", "h"];

                        //* act *//
                        let actual = server.topEight();

                        //* assert *//
                        expect(actual.length).to.equal(expected.length);

                        for (let at = 0; at < expected.length; at++) {
                            expect(actual[at]).to.equal(expected[at]);
                        }
                    }
                );

                this.kit.test("topEight",
                    "When called on an empty graph, an empty array is returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        let server = new GraphServer();
                        server.graph = graph;

                        let expected = [];

                        //* act *//
                        let actual = server.topEight();

                        //* assert *//
                        expect(actual.length).to.equal(expected.length);
                    }
                );

                this.kit.test("topEight",
                    "When called with known realistic edges, the eight vertices with the most edges are returned.",
                    () => {
                        //* arrange *//
                        let graph = new Graph();
                        let server = new GraphServer();
                        server.graph = graph;

                        // region constructing the graph, including its edges

                        this.addGraphVertices(
                            graph,
                            "a", "b", "c", "d", "e", "f", "g", "h",
                            "i", "j", "k", "l", "m", "n", "o", "p"
                        );

                        this.addGraphEdges(
                            graph,
                            {from: "a", to: "d"},
                            {from: "b", to: "e"},
                            {from: "c", to: "f"},
                            {from: "k", to: "f"},
                            {from: "k", to: "g"},
                            {from: "k", to: "n"},
                            {from: "k", to: "o"},
                            {from: "j", to: "o"},
                            {from: "o", to: "p"},
                            {from: "h", to: "i"},
                            {from: "h", to: "j"},
                            {from: "h", to: "g"},
                            {from: "d", to: "n"},
                            {from: "e", to: "m"},
                            {from: "e", to: "p"},
                            {from: "l", to: "p"}
                        )

                        // endregion constructing the graph, including its edges

                        // region calculating the top eight edges using a sort over the whole graph

                        let paired = graph.vertices.map((x, i) => {
                            return ({vertex: x, edges: graph.edges[i]});
                        });

                        // with this lambda, vertices will be sorted from most edges to fewest edges
                        paired = paired.sort((first, second) => {
                            return (second.edges.length - first.edges.length);
                        });

                        // endregion calculating the top eight edges using a sort over the whole graph

                        // the output of topEight() only contains the vertex names
                        let expected = paired
                            .filter((x, i) => i < 8)
                            .map(x => x.vertex);

                        //* act *//
                        let actual = server.topEight();

                        //* assert *//
                        expect(actual.length).to.equal(expected.length);

                        for (let at = 0; at < expected.length; at++) {
                            expect(actual[at]).to.equal(expected[at]);
                        }
                    }
                );

                // endregion topEight()
            }
        );
    }

    // endregion tests


    // region fixtures

    makeSimpleGraphForSingleFind() {
        let graph = new Graph();
        this.addGraphVertices(graph,
            "a", "b", "c", "d", "e", "f", "g", "h"
        );
        graph.addEdge("a", "d");
        graph.addEdge("d", "h");

        return graph;
    }

    makeComplexGraphForRemovals() {
        let graph = new Graph();

        this.addGraphVertices(
            graph,
            "1", "2", "3",
            "4", "5", "6",
            "7", "8", "9"
        );

        this.addGraphEdges(
            graph,
            {from: "4", to: "5"},
            {from: "4", to: "6"},
            {from: "5", to: "6"},
            {from: "6", to: "8"},
            {from: "7", to: "8"}
        );

        return graph;
    }

    addGraphVertices(graph, ...vertices) {
        for (let vertex of vertices) {
            graph.addVertex(vertex);
        }
    }

    addGraphEdges(graph, ...edges) {
        for (let {from, to} of edges) {
            graph.addEdge(from, to);
        }
    }

    equateEntireGraphStructure(expVertices, acVertices, expEdges, acEdges) {
        // first, metadata indicating mutation
        expect(acVertices.length).to.equal(expVertices.length);
        expect(acEdges.length).to.equal(expEdges.length);

        // next, the vertices themselves
        for (let at = 0; at < expVertices.length; at++) {
            expect(acVertices[at]).to.equal(expVertices[at]);
        }

        // next, the edges, but not any other edges; all edges present
        // have changed indices to reflect changed graph contents
        for (let at = 0; at < expEdges.length; at++) {
            // localizing
            let expected = expEdges[at];
            let actual = acEdges[at];

            // metadata for each vertex's edges
            expect(actual.length).to.equal(expected.length);

            // individual elements have to be compared
            for (let of = 0; of < expected.length; of++) {
                expect(actual[of]).to.equal(expected[of]);
            }
        }
    }

    // endregion fixtures
}
