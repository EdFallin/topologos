/**/

import { expect } from "chai";

import { Edge } from "../Edge.js";

describe("Edge", () => {
    describe("static .none", () => {
        it("Returns -1 when retrieved.", /* working */ () => {
            /* Arrange. */
            let expected = -1;

            /* Act. */
            let actual = Edge.none;

            /* Assert. */
            expect(actual).to.equal(expected);
        });
    });
    
    describe("constructor()", () => {
        it("Stores .farEnd if provided.", /* working */ () => {
            /* Arrange. */
            let expected = 12;
            
            /* Act. */
            let edge = new Edge(expected);
            
            /* Assert. */
            expect(edge.farEnd).to.equal(expected);
        });
        
        it("Stores Edge.none if no .farEnd is provided.", /* working */ () => {
            /* No arrange. */

            /* Act. */
            let edge = new Edge();

            /* Assert. */
            expect(edge.farEnd).to.equal(Edge.none);
        });
        
        it("Stores .isForward of true if provided.", /* working */ () => {
            /* Arrange. */
            let expected = true;

            /* Act. */
            let edge = new Edge(12, expected);

            /* Assert. */
            expect(edge.isForward).to.equal(expected);
        });

        it("Stores .isForward of false if provided.", /* working */ () => {
            /* Arrange. */
            let expected = false;

            /* Act. */
            let edge = new Edge(12, expected);

            /* Assert. */
            expect(edge.isForward).to.equal(expected);
        });

        it("Sets .isForward to false if it's not provided.", /* working */ () => {
            /* No arrange. */

            /* Act. */
            let edge = new Edge(12);

            /* Assert. */
            expect(edge.isForward).to.be.false;
        });
    });
    
    describe(".farEnd", () => {
        it("Returns the value it is set to.", /* working */ () => {
            /* Arrange. */
            let edge = new Edge();
            let expected = 17;
            
            /* Act. */
            edge.farEnd = expected;
            
            /* Assert. */
            expect(edge.farEnd).to.equal(expected);
        });
    });
    
    describe(".isForward", () => {
        it("Returns the value it is set to.", /* working */ () => {
            /* Arrange. */
            let edge = new Edge();
            
            /* Act. */
            edge.isForward = true;
            let acTrue = edge.isForward;
            
            edge.isForward = false;
            let acFalse = edge.isForward;
            
            /* Assert. */
            expect(acTrue).to.be.true;
            expect(acFalse).to.be.false;
        });
    });
});
