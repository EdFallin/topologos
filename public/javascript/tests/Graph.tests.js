// /**/
//
import {expect} from "chai";

import {Graph} from "../Graph.js";
import {Vertex} from "../Vertex.js";


// region fixtures

function supplyAbcdVertices() /* verified */ {
    let vertices = [
        new Vertex("a", "a text", 1),
        new Vertex("b", "b text", 2),
        new Vertex("c", "c text", 3),
        new Vertex("d", "d text", 4)
    ];

    return vertices;
}

function supplyBcdeVertices() /* verified */ {
    let vertices = [
        new Vertex("b", "b text", 2),
        new Vertex("c", "c text", 3),
        new Vertex("d", "d text", 4),
        new Vertex("e", "e text", 5)
    ];

    return vertices;
}

function addGraphVertices(graph, vertices) /* verified */ {
    for (let vertex of vertices) {
        graph.addVertex(vertex);
    }
}

// endregion fixtures


// region tests

describe("Graph", () => {
    describe("addVertex()", () => {
        it("Adds a vertex that doesn't exist yet to the given point in .vertices.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();

            let vertex = new Vertex("a", "a text", 1);

            let expVertices = supplyBcdeVertices();
            expVertices.splice(3, 0, vertex);

            let at = 3;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            expect(graph.vertices).to.deep.equal(expVertices);
        });

        it("Adds an empty array for a new vertex to the given point in .edges.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();
            graph.edges = [[6, 4, 2, 7], [6, 2], [7, 3, 8], [2]];

            let vertex = new Vertex("a", "a text", 1);

            let expected = [];

            let at = 3;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            // if vertex added, these should be the same
            expect(graph.edges[at]).to.deep.equal(expected);
        });

        it("Shifts indices upward by one for after-insertion vertices in .edges.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();
            graph.edges = [[1, 4, 2, 7], [6, 0], [7, 3, 8], [2]];

            let vertex = new Vertex("a", "a text", 1);

            let expEdges = [[1, 5, 2, 8], [7, 0], [8, 4, 9], [], [2]];

            let at = 3;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            // if vertex added, these should be the same
            expect(graph.edges).to.deep.equal(expEdges);
        });

        it("Doesn't do anything when the provided vertex already exists.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();
            graph.edges = [[3, 2], [4, 1, 8], [6], []];

            let vertex = new Vertex("c", "c text", 3);

            let expVertices = supplyBcdeVertices();
            let expEdges = [[3, 2], [4, 1, 8], [6], []];

            let at = 1;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            expect(graph.vertices).to.deep.equal(expVertices);
            expect(graph.edges).to.deep.equal(expEdges);
        });

        it("Adds a first new vertex as the sole .vertices element.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = [];
            let vertex = new Vertex("a", "a text", 0);

            let expVertices = [vertex];

            let at = 0;

            //* act *//
            graph.addVertex(vertex, at);

            //* assemble *//
            let acVertices = graph.vertices;

            //* assert *//
            // if vertex added, this length should be right
            expect(acVertices).to.deep.equal(expVertices);
        });

        it("Adds an empty array as the first .edges element for a first new vertex.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = [];
            let vertex = new Vertex("a", "a text", 0);

            let expEdges = [[]];

            let at = 0;

            //* act *//
            graph.addVertex(vertex, at);

            //* assemble *//
            let acEdges = graph.edges;

            //* assert *//
            // if vertex added, this length should be right
            expect(acEdges).to.deep.equal(expEdges);
        });

        it("Adds a vertex to the end when given a point that's too high for .vertices.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();

            let vertex = new Vertex("a", "a text", 1);

            let expVertices = supplyBcdeVertices();
            expVertices.push(vertex);

            // beyond the end 
            let at = graph.vertices.length + 3;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            // if vertex added, these should be the same
            expect(graph.vertices).to.deep.equal(expVertices);
        });

        it("Adds an empty array to the end of .edges when the point is too high for .vertices.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();
            graph.edges = [[6, 4, 2, 7], [6, 2], [7, 3, 8], [2]];

            let vertex = new Vertex("a", "a text", 1);

            let expEdges = [[6, 4, 2, 7], [6, 2], [7, 3, 8], [2], []];

            // beyond the end 
            let at = graph.vertices.length + 7;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            // if vertex added, these should be the same
            expect(graph.edges).to.deep.equal(expEdges);
        });

        it("Adds a vertex at the beginning of .vertices when given a negative point.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();
            graph.edges = [[6, 4, 2, 7], [6, 2], [7, 3, 8], [2]];

            let vertex = new Vertex("a", "a text", 1);

            let expVertices = supplyBcdeVertices();
            expVertices.unshift(vertex);

            // before the start 
            let at = -6;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            // if vertex added, these should be the same
            expect(graph.vertices).to.deep.equal(expVertices);
        });

        it("Adds an empty array at the beginning of .edges when given a negative point.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyBcdeVertices();
            graph.edges = [[6, 4, 2, 7], [6, 2], [7, 3, 8], [2, 0]];

            let vertex = new Vertex("a", "a text", 1);

            let expEdges = [[], [7, 5, 3, 8], [7, 3], [8, 4, 9], [3, 1]];

            // before the start 
            let at = -8;

            //* act *//
            graph.addVertex(vertex, at);

            //* assert *//
            // if vertex added, these should be the same
            expect(graph.edges).to.deep.equal(expEdges);
        });
    });

    describe("addEdge()", () => {
        it("Adds a new edge as the sole element for each vertex when no edges exist.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            let vertices = supplyAbcdVertices();
            addGraphVertices(graph, vertices);

            //* act *//
            graph.addEdge(0, 2);

            //* assert *//
            let expecteds = [[2], [], [0], []];
            expect(graph.edges).to.deep.equal(expecteds);
        });

        it("Adds a new edge for each vertex when edges exist.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyAbcdVertices();
            graph.edges = [[1, 3], [2, 0], [1, 3], [0]];

            //* act *//
            graph.addEdge(0, 2);

            //* assert *//
            let expecteds = [[1, 3, 2], [2, 0], [1, 3, 0], [0]];
            expect(graph.edges).to.deep.equal(expecteds);
        });

        it("Doesn't add an edge between two vertices when it already exists.", /* working */ () => {
            //* arrange *//
            let graph = new Graph();
            graph.vertices = supplyAbcdVertices();
            graph.edges = [[1, 3, 2], [2, 0], [1, 3, 0], [0]];

            //* act *//
            graph.addEdge(0, 2);

            //* assert *//
            let expecteds = [[1, 3, 2], [2, 0], [1, 3, 0], [0]];
            expect(graph.edges).to.deep.equal(expecteds);
        });
    });
});

// endregion tests


// //
// //             this.kit.test("addEdge",
// //                 "When other edges exist, a new edge is added in correct order for each vertex.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "f", "g", "h");
// //
// //                     // edges are vertex indices; using some nonexistent ones here
// //                     graph.edges = [/* f: */ [0, 3], /* g: */ [6, 8, 9], /* h: */ [0, 4, 12, 16]];
// //                     let expecteds = [/* f: */ [0, 3], /* g: */ [2, 6, 8, 9], /* h: */ [0, 1, 4, 12, 16]];
// //
// //                     let expAtG = expecteds[1];
// //                     let expAtH = expecteds[2];
// //
// //                     //* act *//
// //                     graph.addEdge("g", "h");
// //
// //                     //* assemble *//
// //                     let acAtG = graph.edges[1];
// //                     let acAtH = graph.edges[2];
// //
// //                     //* assert *//
// //                     // after insertions, edge arrays for each
// //                     // vertex should both be one longer
// //                     expect(acAtG.length).to.equal(expAtG.length);
// //                     expect(acAtH.length).to.equal(expAtH.length);
// //
// //                     // elements of edge array for first vertex
// //                     for (let of = 0; of < expAtG.length; of++) {
// //                         expect(acAtG[of]).to.equal(expAtG[of]);
// //                     }
// //
// //                     // elements of edge array for second vertex
// //                     for (let of = 0; of < expAtH.length; of++) {
// //                         expect(acAtH[of]).to.equal(expAtH[of]);
// //                     }
// //                 }
// //             );
// //
// //             this.kit.test("addEdge",
// //                 "When edge to be added already exists, edge arrays are not changed.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "f", "g", "h");
// //
// //                     // edges are vertex indices; the "new" edge
// //                     // already exists in its vertices' arrays;
// //                     // also using some nonexistent indices here
// //                     graph.edges = [/* f: */ [0, 3], /* g: */ [2, 6, 8, 9], /* h: */ [0, 1, 4, 12, 16]];
// //                     let expecteds = [/* f: */ [0, 3], /* g: */ [2, 6, 8, 9], /* h: */ [0, 1, 4, 12, 16]];
// //
// //                     let expAtG = expecteds[1];
// //                     let expAtH = expecteds[2];
// //
// //                     //* act *//
// //                     graph.addEdge("g", "h");
// //
// //                     //* assemble *//
// //                     let acAtG = graph.edges[1];
// //                     let acAtH = graph.edges[2];
// //
// //                     //* assert *//
// //                     // after insertions, edge arrays for each
// //                     // vertex should both be one longer
// //                     expect(acAtG.length).to.equal(expAtG.length);
// //                     expect(acAtH.length).to.equal(expAtH.length);
// //
// //                     // elements of edge array for first vertex
// //                     for (let of = 0; of < expAtG.length; of++) {
// //                         expect(acAtG[of]).to.equal(expAtG[of]);
// //                     }
// //
// //                     // elements of edge array for second vertex
// //                     for (let of = 0; of < expAtH.length; of++) {
// //                         expect(acAtH[of]).to.equal(expAtH[of]);
// //                     }
// //                 }
// //             );
// // });
//
//
// //
// //             // region findVertex()
// //
// //             this.kit.test("findVertex",
// //                 "When sought vertex doesn't exist, null is returned.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d", "e", "f", "g", "h");
// //                     graph.addEdge("a", "d");
// //                     graph.addEdge("d", "h");
// //
// //                     let expected = null;
// //
// //                     //* act *//
// //                     let actual = graph.findVertex("q");
// //
// //                     //* assert *//
// //                     expect(actual).to.equal(expected);
// //                 }
// //             );
// //
// //             this.kit.test("findVertex",
// //                 "When sought vertex exists, its index and edges are returned.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d", "e", "f", "g", "h");
// //                     graph.addEdge("a", "d");
// //                     graph.addEdge("d", "h");
// //
// //                     // "d" is at 3 and has edges to "a" at 0 and "h" at 7
// //                     let expected = {index: 3, edges: [0, 7]};
// //
// //                     //* act *//
// //                     let actual = graph.findVertex("d");
// //
// //                     //* assert *//
// //                     expect(actual.index).to.equal(expected.index);
// //                     expect(actual.edges.length).to.equal(expected.edges.length);
// //
// //                     for (let of = 0; of < expected.edges.length; of++) {
// //                         expect(actual.edges[of]).to.equal(expected.edges[of]);
// //                     }
// //                 }
// //             );
// //
// //             // endregion findVertex()
// //
// //             // region findVertices()
// //
// //             this.kit.test("findVertices",
// //                 "When vertices are sought by two other vertices, the correct vertices are returned.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d", "e", "f", "g", "h");
// //
// //                     // b neighbors a, f, and h; e neighbors a and h;
// //                     // vertices a and h are neighbors of both b and e
// //                     this.addGraphEdges(graph, {from: "b", to: "a"}, {from: "b", to: "f"}, {from: "b", to: "h"});
// //                     this.addGraphEdges(graph, {from: "e", to: "a"}, {from: "e", to: "h"});
// //
// //                     let expecteds = [{name: "a", edges: [1, 4]}, {name: "h", edges: [1, 4]}];
// //
// //                     //* act *//
// //                     let actuals = graph.findVertices("b", "e");
// //
// //                     //* assert *//
// //                     // the right number of vertices should have been returned
// //                     expect(actuals.length).to.equal(expecteds.length);
// //
// //                     // each of the vertices returned should have the right name and edges
// //                     for (let of = 0; of < expecteds.length; of++) {
// //                         // name
// //                         expect(actuals[of].name).to.equal(expecteds[of].name);
// //                         expect(actuals[of].edges.length).to.equal(expecteds[of].edges.length);
// //
// //                         // localizing edges
// //                         let expEdges = expecteds[of].edges;
// //                         let acEdges = actuals[of].edges;
// //
// //                         // edges
// //                         for (let at = 0; at < expEdges.length; at++) {
// //                             expect(acEdges[at]).to.equal(expEdges[at]);
// //                         }
// //                     }
// //                 }
// //             );
// //
// //
// //             this.kit.test("findVertices",
// //                 "When vertices are sought by three other vertices, the correct vertices are returned.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph,
// //                         "a", "b", "c", "d", "e", "f", "g",
// //                         "h", "i", "j", "k", "l", "m"
// //                     );
// //
// //                     // b neighbors a, f, g, and h
// //                     this.addGraphEdges(graph,
// //                         {from: "b", to: "a"}, {from: "b", to: "f"},
// //                         {from: "b", to: "g"}, {from: "b", to: "h"}
// //                     );
// //
// //                     // e neighbors a, c, g, and h
// //                     this.addGraphEdges(graph,
// //                         {from: "e", to: "a"}, {from: "e", to: "c"},
// //                         {from: "e", to: "g"}, {from: "e", to: "h"}
// //                     );
// //
// //                     // d neighbors a, and g
// //                     graph.addEdge("d", "a");
// //                     graph.addEdge("d", "g");
// //
// //                     // a few extra neighbors for fuller edge arrays in output, including between the founds
// //                     this.addGraphEdges(graph,
// //                         {from: "a", to: "j"}, {from: "g", to: "l"},
// //                         {from: "a", to: "k"}, {from: "g", to: "m"},
// //                         {from: "a", to: "i"}, {from: "a", to: "g"}
// //                     );
// //
// //                     // a and g are the only neighbors of all of b, e, and d;
// //                     // their edges include all their neighbors, sought or not
// //                     let expecteds = [
// //                         {name: "a", edges: [1, 3, 4, 6, 8, 9, 10]},
// //                         {name: "g", edges: [0, 1, 3, 4, 11, 12]}
// //                     ];
// //
// //                     //* act *//
// //                     let actuals = graph.findVertices("e", "d", "b");
// //
// //                     //* assert *//
// //                     // the right number of vertices should have been returned
// //                     expect(actuals.length).to.equal(expecteds.length);
// //
// //                     // each of the vertices returned should have the right name and edges
// //                     for (let of = 0; of < expecteds.length; of++) {
// //                         // name
// //                         expect(actuals[of].name).to.equal(expecteds[of].name);
// //                         expect(actuals[of].edges.length).to.equal(expecteds[of].edges.length);
// //
// //                         // localizing edges
// //                         let expEdges = expecteds[of].edges;
// //                         let acEdges = actuals[of].edges;
// //
// //                         // edges
// //                         for (let at = 0; at < expEdges.length; at++) {
// //                             expect(acEdges[at]).to.equal(expEdges[at]);
// //                         }
// //                     }
// //                 }
// //             );
// //
// //             // endregion findVertices()
// //
// //             // region removeEdge()
// //
// //             this.kit.test("removeEdge",
// //                 "When an edge is removed, it is no longer found in either vertex's .edges array.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d");
// //                     graph.addEdge("a", "b");
// //                     graph.addEdge("a", "c");
// //                     graph.addEdge("a", "d");
// //                     graph.addEdge("b", "d");
// //
// //                     let expEdgesFromA = [1, 2];
// //                     let expEdgesFromD = [1];
// //
// //                     //* act *//
// //                     graph.removeEdge("a", "d");
// //
// //                     //* assert *//
// //                     expect(graph.edges[0].length).to.equal(expEdgesFromA.length);
// //
// //                     for (let at = 0; at < expEdgesFromA.length; at++) {
// //                         let expected = expEdgesFromA[at];
// //                         let actual = graph.edges[0][at];
// //
// //                         expect(actual).to.equal(expected);
// //                     }
// //
// //                     expect(graph.edges[3].length).to.equal(expEdgesFromD.length);
// //
// //                     for (let at = 0; at < expEdgesFromD.length; at++) {
// //                         let expected = expEdgesFromD[at];
// //                         let actual = graph.edges[3][at];
// //
// //                         expect(actual).to.equal(expected);
// //                     }
// //                 }
// //             );
// //
// //             // endregion removeEdge()
// //
// //             // region removeVertex()
// //
// //             this.kit.test("removeVertex",
// //                 "When a vertex is removed, it is no longer present in the graph's .vertices array.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d");
// //                     this.addGraphEdges(graph, {from: "a", to: "c"}, {from: "b", to: "d"});
// //
// //                     let expecteds = ["a", "c", "d"];
// //
// //                     //* act *//
// //                     graph.removeVertex("b");
// //
// //                     //* assert *//
// //                     expect(graph.vertices.length).to.equal(expecteds.length);
// //
// //                     for (let at = 0; at < expecteds.length; at++) {
// //                         let expected = expecteds[at];
// //                         let actual = graph.vertices[at];
// //
// //                         expect(actual).to.equal(expected);
// //                     }
// //                 }
// //             );
// //
// //             this.kit.test("removeVertex",
// //                 "When a vertex is removed, edges to and from it are also removed.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d");
// //                     this.addGraphEdges(
// //                         graph,
// //                         {from: "a", to: "c"},
// //                         {from: "b", to: "c"},
// //                         {from: "b", to: "a"},
// //                         {from: "d", to: "b"},
// //                         {from: "c", to: "d"}
// //                     );
// //
// //                     let expAtA = [1, 2];
// //                     let expAtB = [0, 2];
// //                     let expAtC = [0, 1];
// //
// //                     //* act *//
// //                     // removing the last vertex so that there
// //                     // are no later indices to be changed here
// //                     graph.removeVertex("d");
// //
// //                     //* assert *//
// //                     expect(graph.edges[0].length).to.equal(expAtA.length);
// //                     expect(graph.edges[1].length).to.equal(expAtB.length);
// //                     expect(graph.edges[2].length).to.equal(expAtC.length);
// //
// //                     let expecteds = [expAtA, expAtB, expAtC];
// //                     let actuals = [graph.edges[0], graph.edges[1], graph.edges[2]];
// //
// //                     for (let at = 0; at < expecteds.length; at++) {
// //                         let expected = expecteds[at];
// //                         let actual = actuals[at];
// //
// //                         for (let of = 0; of < expected.length; of++) {
// //                             expect(actual[of]).to.equal(expected[of]);
// //                         }
// //                     }
// //                 }
// //             );
// //
// //             this.kit.test("removeVertex",
// //                 "When a vertex is removed, any edge to a higher-index vertex is decremented by one.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d", "e");
// //                     graph.addEdge("a", "e");
// //
// //                     let expAtA = [3];
// //                     let expAtE = [0];
// //
// //                     //* act *//
// //                     // removing the last vertex so that there
// //                     // are no later indices to be changed here
// //                     graph.removeVertex("b");
// //
// //                     //* assert *//
// //                     expect(graph.edges[0].length).to.equal(expAtA.length);
// //                     expect(graph.edges[3].length).to.equal(expAtE.length);
// //
// //                     let expecteds = [expAtA, expAtE];
// //                     let actuals = [graph.edges[0], graph.edges[3]];
// //
// //                     for (let at = 0; at < expecteds.length; at++) {
// //                         let expected = expecteds[at];
// //                         let actual = actuals[at];
// //
// //                         for (let of = 0; of < expected.length; of++) {
// //                             expect(actual[of]).to.equal(expected[of]);
// //                         }
// //                     }
// //                 }
// //             );
// //
// //             this.kit.test("removeVertex",
// //                 "When a vertex is removed, only edges to remaining vertices exist, and they retain the correct indices.",
// //                 () => {
// //                     //* arrange *//
// //                     let graph = new Graph();
// //                     this.addGraphVertices(graph, "a", "b", "c", "d", "e");
// //                     this.addGraphEdges(
// //                         graph,
// //                         {from: "a", to: "c"},
// //                         {from: "a", to: "b"},
// //                         {from: "a", to: "d"},
// //                         {from: "a", to: "e"},
// //                         {from: "b", to: "c"},
// //                         {from: "b", to: "d"},
// //                         {from: "b", to: "e"},
// //                         {from: "c", to: "d"},
// //                         {from: "c", to: "e"}
// //                     );
// //
// //                     let expAtA = [1, 2, 3];
// //                     let expAtC = [0, 2, 3];
// //                     let expAtD = [0, 1];
// //                     let expAtE = [0, 1];
// //
// //                     //* act *//
// //                     // removing the last vertex so that there
// //                     // are no later indices to be changed here
// //                     graph.removeVertex("b");
// //
// //                     //* assert *//
// //                     expect(graph.edges[0].length).to.equal(expAtA.length);
// //                     expect(graph.edges[1].length).to.equal(expAtC.length);
// //                     expect(graph.edges[2].length).to.equal(expAtD.length);
// //                     expect(graph.edges[3].length).to.equal(expAtE.length);
// //
// //                     let expecteds = [
// //                         expAtA, expAtC, expAtD, expAtE
// //                     ];
// //                     let actuals = [
// //                         graph.edges[0], graph.edges[1],
// //                         graph.edges[2], graph.edges[3]
// //                     ];
// //
// //                     for (let at = 0; at < expecteds.length; at++) {
// //                         let expected = expecteds[at];
// //                         let actual = actuals[at];
// //
// //                         for (let of = 0; of < expected.length; of++) {
// //                             expect(actual[of]).to.equal(expected[of]);
// //                         }
// //                     }
// //                 }
// //             );
// //
// //             // endregion removeVertex()
// //
// //         }
// //     );
// // }
//
//
//
// // // region fixtures
// //
//
// //
// // addGraphEdges(graph, ...edges) {
// //     for (let {from, to} of edges) {
// //         graph.addEdge(from, to);
// //     }
// // }
// //
// // equateArrays(firstArray, secondArray) {
// //     // equal arrays have the same lengths
// //     expect(firstArray.length).to.equal(secondArray.length);
// //
// //     // equal arrays have the same elements at each index
// //     for (let at = 0; at < firstArray.length; at++) {
// //         expect(firstArray[at]).to.equal(secondArray[at]);
// //     }
// // }
// //
// // // endregion fixtures
// // }
