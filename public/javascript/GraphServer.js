/* imports of dependencies, including standard file-related
   ones, which can also be loaded with require() in CJS */
import {Graph} from "./Graph.js";
import fs from "fs";
import path from "path";

export class GraphServer {
    // region definitions

    static NO_SUCH_FILE = "ENOENT";
    static NO_VERTEX = -1;

    static ROOT_FOLDER = "./public/graphs";

    // endregion definitions


    // region constructor and dependencies

    constructor(graphFile) {
        // // a constant value needed for path correction
        // this.graphRootRegex = /\/\w:/;

        let mode = fs.constants.F_OK | fs.constants.S_IFDIR;

        // future, solidify this
        let doesExist = true;
        try {
            fs.accessSync(GraphServer.ROOT_FOLDER, mode);
            doesExist = true;
        } catch {
            doesExist = false;
        }

        if (!doesExist) {
            fs.mkdirSync(GraphServer.ROOT_FOLDER);
        }

        // components of the site for the graph
        // this.calculateGraphRoot();

        this.graphFolder = GraphServer.ROOT_FOLDER;
        this.graphFile = graphFile;
    }

    // endregion constructor and dependencies


    // region Graph saving

    //* calculated property getter; no matching setter here *//
    get graphSite() {
        let graphSite = path.join(/*this.graphRoot,*/ this.graphFolder, this.graphFile);
        return graphSite;
    }

    writeGraphToFile() {
        let asJson = JSON.stringify(this.graph);
        fs.writeFileSync(this.graphSite, asJson);
    }

    // endregion Graph saving


    // region static convenience initor

    //* inits a GraphServer with a Graph from the arg file *//
    static fromFile(graphFile) {
        let state = new GraphServer(graphFile);
        let toRead = state.graphSite;

        let asJson = null;

        /* read the file, but if it doesn't exist, init state
           with an empty Graph, save that, and return state */
        try {
            asJson = fs.readFileSync(toRead, "utf-8");
        }
        catch (thrown) {
            if (thrown.code === GraphServer.NO_SUCH_FILE) {
                // empty Graph and a file of it
                state.graph = new Graph();
                state.writeGraphToFile();

                // to caller
                return state;
            }

            // no sensible handling here for any other throw
            throw thrown;
        }

        // returned value is JSON, so I have to deserialize it to
        // a plain JS object, then convert that to my Graph type
        let plain = JSON.parse(asJson);
        let graph = Graph.fromContents(plain.vertices, plain.edges, plain.visits);

        state.graph = graph;

        // to caller
        return state;
    }

    // endregion static convenience initor


    // region methods addressing the internal Graph

    //* future: remove edges, remove vertices *//

    topEight() {
        /* algorithm: traversal of all edges, sorting into a pre-sized output using a quasi-insertion sort,
        *  with shifting endward when latest is disordered; O(n²), but only over output, so most efficient  */

        // initing the output with all default values, lowest to work for sorting
        let countsByVertex = Array(8);
        countsByVertex.fill({vertex: GraphServer.NO_VERTEX, edges: 0});

        // simplifying later code
        let top = countsByVertex.length - 1;

        // traversal over values, and sorting into the array, then sorting in when suitable;
        // downward from the top vertex so that the sort is stable relative to entry order
        for (let vertex = this.graph.edges.length - 1; vertex >= 0; vertex--) {
            // current count of edges to work with
            let edges = this.graph.edges[vertex].length;

            // look across output
            for (let at = 0; at < countsByVertex.length; at++) {
                // if disordered, take sort-into-output steps
                if (edges >= countsByVertex[at].edges) {
                    // shifting endward, from the end so the first isn't copied to all
                    for (let to = top, from = top - 1; from >= at; from--, to--) {
                        countsByVertex[to] = countsByVertex[from];
                    }

                    // inserting the current vertex / edge where it belongs for now
                    countsByVertex[at] = {vertex, edges};

                    // stopping after the first shift; all endward are lower
                    break;
                }
            }
        }

        // stripping any default fake vertices
        countsByVertex = countsByVertex.filter(x => {
            return x.vertex !== GraphServer.NO_VERTEX;
        });

        // converting to just the vertices
        let topEight = countsByVertex.map(x => {
            let vertex = x.vertex;
            return this.graph.vertices[vertex];
        });

        // to caller
        return topEight;
    }

    find(factors) {
        let found = null;

        //* fail path: same result as nothing found *//
        if (factors.length === 0) {
            return [];
        }

        //* main path: if one fact, findVertex(); if two or more, findVertices() *//

        if (factors.length === 1) {
             found = this.graph.findVertex(factors[0]);

             if (found != null) {
                 let name = this.graph.vertices[found.index];
                 let edges = found.edges;
                 found = [{name, edges}];
             }
        }
        else {
            /* to reuse a "rest parameter" as an argument,
               just put the ellipsis in front of it again */
            found = this.graph.findVertices(...factors);
        }

        if (found == null) {
            return [];
        }

        return found;
    }

    add(bundle) {
        /* adding vertices with addVertex() 1+ times,
           then their edges with addEdge() 0/1+ times */

        // each vertex is added, and any edges from it
        for (let node of bundle.nodes) {
            let vertex = node.vertex;
            this.graph.addVertex(vertex);

            for (let edge of node.edges) {
                this.graph.addEdge(vertex, edge);
            }
        }

        // the file is always saved with new changes
        this.writeGraphToFile();
    }

    remove(factors) {
        /* algorithm: get each vertex or edge, see which it is,
           call the Graph method that removes it as needed,
           then call the code to save the changes to the file */

        // cycling over each factor to be deleted
        for (let fact of factors) {
            if (fact.of) {
                this.graph.removeVertex(fact.of);
            }

            if (fact.from) {
                this.graph.removeEdge(fact.from, fact.to);
            }
        }

        //* future: implement this, and change tests to use a temporary file *//
        // the file is always saved with new changes
        this.writeGraphToFile();
    }

    // endregion methods addressing the internal Graph

}
