/**/

export class Edge {
    static get none() /* passed */ {
        return -1;
    }
    
    constructor(farEnd, isForward) /* passed */ {
        this._farEnd = farEnd || Edge.none;
        this._isForward = isForward || false;
    }
    
    // region .farEnd
    
    get farEnd() /* passed */ {
        return this._farEnd;
    }
    
    set farEnd(farEnd) /* passed */ {
        this._farEnd = farEnd;
    }
    
    // endregion .farEnd
    
    
    // region .isForward
    
    get isForward() /* passed */ {
        return this._isForward;
    }
    
    set isForward(isForward) /* passed */ {
        this._isForward = isForward;
    }
    
    // endregion .isForward
    
}
