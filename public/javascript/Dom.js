/**/

export class Dom {
    constructor(dom) {
        this.dom = dom;
        
        this.displayStyle = this.dom.getElementById("DisplayStyle");
        this.asGraph = this.dom.getElementById("AsGraph");
        this.asText = this.dom.getElementById("AsText");
        
        this.asStyles = [ this.asGraph, this.asText ];
    }
    
}
